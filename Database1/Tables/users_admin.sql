﻿CREATE TABLE [dbo].[users_admin]
(
	[Root]		int				Not Null, -- 0-> wpf   1-> mobile  2 -> else 
	[Email]		char(50)		Not Null,
	[UserName]	char(50)		Not Null,
	[FullName]	char(50)		Null,
	[Password]	char(50)		Not Null,
	[BirdtDay]	datetime2		Not Null,
	[Tell]		char(20)		Null,
	[Img]		varbinary(MAX)	Null

	Constraint  Pk_user Primary Key([Email], [UserName])
)
