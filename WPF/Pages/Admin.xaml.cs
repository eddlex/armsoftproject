﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Qr.Pages
{
    /// <summary>
    /// Interaction logic for Admin.xaml
    /// </summary>
    public partial class Admin : Page
    {
        public Admin()
        {
            InitializeComponent();
        }

        private void Label_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.NavigationService.Navigate(new Exit());
        }

        private void Settings_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

           ToolsPanel.Content = null;
           ToolsPanel.SetValue(Grid.RowProperty, 1);
            ToolsPanel.SetValue(Grid.RowSpanProperty, 6);

            ToolsPanel.SetValue(Grid.ColumnProperty, 2);
            ToolsPanel.SetValue(Grid.ColumnSpanProperty, 3);
            
            ToolsPanel.Margin = new Thickness(0, 0, 0, 0);
            ToolsPanel.Height = 810;
            ToolsPanel.Width = 950;
            ToolsPanel.Content = new Settings();


        }
        private void Help_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ToolsPanel.Content = null;
            ToolsPanel.SetValue(Grid.RowProperty, 2);
            ToolsPanel.SetValue(Grid.RowSpanProperty, 4);
            //Grid.Column = "2" Grid.ColumnSpan = "3" Grid.Row = "2" Grid.RowSpan = "4"
            ToolsPanel.SetValue(Grid.ColumnProperty, 2);
            ToolsPanel.SetValue(Grid.ColumnSpanProperty, 3);
            ToolsPanel.Margin = new Thickness(0, 0, 0, 0);
            ToolsPanel.Height = 553;
            ToolsPanel.Width = 950;
            ToolsPanel.Content = new Help();

        }

        private void Home_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ToolsPanel.Content = null;
            ToolsPanel.Height = 960;
            ToolsPanel.Width = 1560;
            ToolsPanel.Margin = new Thickness(0, 10, 50, 0);
       
            //Margin = "292,50,0,0"  VerticalAlignment = "Top" Width = "1596" Fill = "#FF040202" RadiusX = "20" RadiusY = "21
            ToolsPanel.SetValue(Grid.RowProperty, 0);
            ToolsPanel.SetValue(Grid.RowSpanProperty, 8);  
            ToolsPanel.SetValue(Grid.ColumnProperty, 0);
            ToolsPanel.SetValue(Grid.ColumnSpanProperty, 6);
            ToolsPanel.Content = new Home();
        }
    }
}
