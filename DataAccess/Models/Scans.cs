﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    internal class Scans
    {
		//[Link] char (1000)		Not Null,

		//[Name]          char (50)		Null,
		//[LastName] char (50)		Null,
		//[Serteficate] char (50)		Null, 
		//[Valid] int Null, -- 1true  0false
		//[Geo] char (50)		Not Null,

		//[BirdtDay]      datetime2 Null,

		//[Date]          datetime2 Not Null,
		//[Country] char (20)		Not NULL,

		//[Ip]            char (20)		Not Null,

		//[UserAgent]     char (100)		Not Null,

		// [Img]           varbinary(MAX)  Null,
		//[Else] char (50)        NUll

		
		public string	Name		{ get; set; }
		public string	LastName	{ get; set; }
		public DateTime BirtDay		{ get; set; }
		public string	Country		{ get; set; }
		public byte[]?	Img { get; set; }

		public string	Link { get; set; }
		public string	Serteficate { get; set; }
		public int		 Valid { get; set; }

		public string	Ip			{ get; set; }
		public string	UserAgent	{ get; set; }
		public string	Geo	{ get; set; }
	}
}
